# ListApp
## To run this project

1. Clone this repo 
```
git clone https://github.com/vlade2008/ListApp.git
```
2. cd into directory and install the dependencies
```
cd ListApp
yarn install
```
3. cd ios and install pods
```
cd ios
pod install
```
4. Run Project

Running Ios
```
yarn ios
```

Running Android
```
yarn android
```

